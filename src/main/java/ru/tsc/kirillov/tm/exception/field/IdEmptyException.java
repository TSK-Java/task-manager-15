package ru.tsc.kirillov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Ошибка! ID не задан.");
    }

    public IdEmptyException(final String objName) {
        super("Ошибка! [" + objName + "] ID не задан.");
    }
}
